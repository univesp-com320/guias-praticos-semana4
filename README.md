# Guias Praticos Semana4

Os guias a seguir tem por objetivo discutir alguns aspectos relativos ao  banco de dados MySQL que vamos utilizar para desenvolvimento do CRUD com Laravel e Angular. O principal objetivo é criar uma API que realiza CRUD de País e Universidades. Escolha o guia de sua preferência, de acordo com o sistema operacional que você está utilizando: Linux Ubuntu 20.04 ou Windows (64 Bits).

## Modelagem do Banco de Dados da Aplicação

Vamos considerar, para nossa aplicação, um pequno banco de dados com algumas tabelas, para que possamos conectar o front-end e o back-end e termos uma aplicação funcional, em que o back-end interaja com o banco de dados por meio de requisições/respostas HTTP feitas pelo front-end. Apresento a seguir, um modelo entidade e relacionamento conforme a figura abaixo:

<p align="center">
  <img target="_blank" src="./imagens/modelo_relacionamento_univesp.drawio.png" alt="Modelo de entidade e relacionamento."/>
</p>

Este modelo contém três tabelas: `Usuário`, `País` e `Universidade`. A tabela de usuários possui campos que são importantes para garantir o acesso dos usuários aos dados da aplicação. Embora tenhamos apresentado esta tabela, não iremos implementar uma tela de login neste caso. Temos também a tabela Universidade, onde cada Universidade está vinculada a um país e, dessa forma, existe uma chave estrangeira de país em Universidade. É com base neste exemplo do modelo que vamos poder exibir como são mapeadas as entidades relacionadas em Laravel.

## Apresentação das funcionalidades do MySQL no Laragon - Ambiente Windows (Versão 64 Bits)

Vamos utilizar todas as funcionalidades do framework Laragon, que foi discutido na segunda semana do curso. Este framework, como já discutido, engloba PHP, Apache, MySQL e diversas outras ferramentas, o que facilita o desenvolvimento da aplicação Web.

### Serviço MySQL

A tela inicial do Laragon é bem simples, de modo que, quando iniciamos o framework, ele nos dá opções para inicializar/parar serviços. No caso do MySQL, para ser mais fácil, basta clicar no botão como mostra a figura a seguir:

<p align="center">
  <img target="_blank" src="./imagens/mysql1.png" alt="MySQL na tela do Laragon."/>
</p>

Após inicializar os serviços do MySQL e Apache, teremos como resultado a tela abaixo:

<p align="center">
  <img target="_blank" src="./imagens/mysql2.png" alt="MySQL na tela do Laragon."/>
</p>

Podemos então partir para a criação da base de dados que será usada na Semana 5, em que abordaremos o desenvolvimento do back-end. Para criar esta base de dados, clique com o botão direito do mouse na tela do Laragon e terá como resultado a tela a seguir:

<p align="center">
  <img target="_blank" src="./imagens/mysql3.png" alt="MySQL na tela do Laragon."/>
</p>

Escolha a opção **Criar banco de dados** e terá como resultado a seguinte tela:

<p align="center">
  <img target="_blank" src="./imagens/mysql4.png" alt="MySQL na tela do Laragon."/>
</p>

No campo que será apresentado, digite: **laravel_db**.

<p align="center">
  <img target="_blank" src="./imagens/mysql5.png" alt="MySQL na tela do Laragon."/>
</p>
 
Na sequência, vamos abrir o Terminal para verificarmos se a base de dados foi criada adequadamente.

<p align="center">
  <img target="_blank" src="./imagens/mysql6.png" alt="MySQL na tela do Laragon."/>
</p>

Digite no terminal o comando: **mysql -u root**, como mostra a figura abaixo:

<p align="center">
  <img target="_blank" src="./imagens/mysql7.png" alt="MySQL na tela do Laragon."/>
</p>

Para finalizar, digite o comando **show databases**; e terá como resultado a listagem das bases de dados já criadas. Veja que a base de dados **laravel_db** está nesta lista. Pronto!!!

## Apresentação das funcionalidades do MySQL no Linux Ubuntu 20.04

No caso do Linux, vamos atuar diretamente no prompt de comando e chamar o MySQL. Assumo que você já tem o MySQL instalado, conforme discutido no guia prático da Semana 2. Desta forma, farei neste guia apenas uma revisão para nos certificarmos de que tudo está OK com o funcionamento do MySQL, da criação do banco de dados e da interação deste banco com o servidor Web Apache.

### Serviço MySQL

Considerando que você teve sucesso na instalação do MySQL na Semana 2, o serviço é inicializado sempre que o sistema operacional é iniciado (bootado). No Linux, para verificarmos se um serviço está operacional (ligado), utilizamos o comando **systemctl** no terminal. Eu utilizo um terminal chamado terminator (se você quiser também utilizá-lo utilize este [Link](https://www.linuxandubuntu.com/home/terminator-a-linux-terminal-emulator-with-multiple-terminals-in-one-window). Abra o terminal como mostra a figura abaixo:

<p align="center">
  <img target="_blank" src="./imagens/mysql0-linux.png" alt="MySQL na tela do Laragon."/>
</p>

No caso para verificarmos o serviço MySQL, digite o comando a seguir:

```
systemcl status mysql
```
E teremos a saída como mostrado abaixo:

<p align="center">
  <img target="_blank" src="./imagens/mysql1-linux.png" alt="Status do MySQL"/>
</p>

Podemos então partir para a criação da base de dados que será usada na Semana 5, em que abordaremos o desenvolvimento do back-end. Para criar esta base de dados, vamos continuar no terminal. Digite no terminal o comando: **mysql -u root - p**, e forneça sua senha caso tenha definido na instalação do MySQL. A saída é dada como mostra a figura abaixo:

<p align="center">
  <img target="_blank" src="./imagens/mysql2-linux.png" alt="Acesso ao MySQL."/>
</p>

Para criar um banco de dados, digite no prompt do MySQL **create database laravel_db;** e terá como resultado a seguinte tela:

<p align="center">
  <img target="_blank" src="./imagens/mysql4-linux.png" alt="Comando para criar a base."/>
</p>

Para finalizar, ainda no terminal, digite o comando **show databases**; e terá como resultado a listagem das bases de dados já criadas. Veja que a base de dados **laravel_db** está nesta lista. Pronto!!!

<p align="center">
  <img target="_blank" src="./imagens/mysql3-linux.png" alt="Base criada."/>
</p>

Desta forma estaremos preparados para tratar do desenvolvimento do back-end com o Laravel. Até a próxima!!!
